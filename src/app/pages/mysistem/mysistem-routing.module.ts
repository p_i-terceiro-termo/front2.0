import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MysistemComponent } from './mysistem.component';

const routes: Routes = [{ path: '', component: MysistemComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MysistemRoutingModule { }
