import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EChartsOption } from 'echarts';
import { ECharts } from 'echarts/types/dist/echarts';
import { NgxEchartsDirective } from 'ngx-echarts';
import { FeedingComponent } from 'src/app/components/feeding/feeding.component';


// export enum OxigenComponentStates {
//   INIT,
//   LOADING,
//   READY,
// }

@Component({
  selector: 'app-mysistem',
  templateUrl: './mysistem.component.html',
  styleUrls: ['./mysistem.component.scss']
})
export class MysistemComponent implements OnInit {
  // state: OxigenComponentStates = OxigenComponentStates.INIT;
  // states = OxigenComponentStates;
  option: EChartsOption = {};

  constructor(private modalService: NgbModal){
  }

  @ViewChild(`chart`) echartsInstance: ECharts | null = null;

  ngOnInit(): void {
    this.loadData();
  }

  openModal(): void{
    this.modalService.open(FeedingComponent)
  }

  onChartInit(ec: any) {
    this.echartsInstance = ec;
  }

  async loadData(): Promise<void> {
    // this.state = OxigenComponentStates.LOADING;
    const oxygenValue = await this.getDataFromAPI();
    this.setOptions(oxygenValue);
    // this.state = OxigenComponentStates.READY;
  }

  getDataFromAPI(): Promise<number> {
    return new Promise((resolve, reject) => {
      const random = +(Math.random() * 80).toFixed(2);
      setTimeout(() => resolve(random), 2000);
    });
  }

  setGrapData(value: number) {
    const newOpt: EChartsOption = {
      series: [
        {
          data: [
            {
              value: 4,
            },
          ],
        },
      ],
    };
    this.echartsInstance?.setOption(newOpt);
  }

  private setOptions(value:number) {
    this.option = {
      series: [
        {
          type: 'gauge',
          center: ['50%', '60%'],
          startAngle: 190,
          endAngle: -10,

          min: 0,
          max: 100,
          //divisoes do total
          splitNumber: 5,
          itemStyle: {
            //cor do nivel
            color: '#59EB57',
          },
          progress: {
            show: true,
            width: 30,
          },

          pointer: {
            show: false,
          },
          axisLine: {
            lineStyle: {
              width: 30,
            },
          },
          axisTick: {
            show: false,
            distance: -45,
            splitNumber: 1,
            lineStyle: {
              width: 2,
              color: '#000000',
            },
          },
          splitLine: {
            show: false,
            distance: -50,
            length: 0,
            lineStyle: {
              width: 3,
              color: '#999',
            },
          },
          axisLabel: {
            show: false,
            distance: -20,
            color: '#000000',
            fontSize: 20,
          },
          anchor: {
            show: false,
          },
          title: {
            show: false,
          },
          detail: {
            valueAnimation: true,
            width: '60%',
            lineHeight: 40,
            borderRadius: 8,
            offsetCenter: [0, '-15%'],
            fontSize: 25,
            fontWeight: 'bolder',
            formatter: '{value} %',
            color: 'inherit',
          },
          data: [
            {
              value,
            },
          ],
        },
      ]
    }
  }
}
