import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MysistemRoutingModule } from './mysistem-routing.module';
import { MysistemComponent } from './mysistem.component';
import { ModaisModule } from 'src/app/components/modais.module';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgbAlertModule, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    MysistemComponent
  ],
  imports: [
    CommonModule,
    MysistemRoutingModule,
    ModaisModule,
    NgxEchartsModule.forRoot({
      /**
       * This will import all modules from echarts.
       * If you only need custom modules,
       * please refer to [Custom Build] section.
       */
      echarts: () => import('echarts'), // or import('./path-to-my-custom-echarts')
    }),
    NgbPaginationModule,
    NgbAlertModule
  ]
})
export class MysistemModule { }
