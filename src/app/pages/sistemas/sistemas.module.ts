import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SistemasRoutingModule } from './sistemas-routing.module';
import { SistemasComponent } from './sistemas.component';


@NgModule({
  declarations: [
    SistemasComponent
  ],
  imports: [
    CommonModule,
    SistemasRoutingModule
  ]
})
export class SistemasModule { }
