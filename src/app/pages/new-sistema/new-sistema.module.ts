import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewSistemaRoutingModule } from './new-sistema-routing.module';
import { NewSistemaComponent } from './new-sistema.component';


@NgModule({
  declarations: [
    NewSistemaComponent
  ],
  imports: [
    CommonModule,
    NewSistemaRoutingModule
  ]
})
export class NewSistemaModule { }
