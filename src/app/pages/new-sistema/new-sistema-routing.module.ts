import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewSistemaComponent } from './new-sistema.component';

const routes: Routes = [{ path: '', component: NewSistemaComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewSistemaRoutingModule { }
