import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LimpezaComponent } from './limpeza/limpeza.component';
import { FeedingComponent } from './feeding/feeding.component';



@NgModule({
  declarations: [
    LimpezaComponent,
    FeedingComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    LimpezaComponent,
    FeedingComponent
  ]
})
export class ModaisModule { }
