import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/login/login.module').then((m) => m.LoginModule),
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./pages/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'register',
    loadChildren: () =>
      import('./pages/register/register.module').then((m) => m.RegisterModule),
  },
  { path: 'sistemas', loadChildren: () => import('./pages/sistemas/sistemas.module').then(m => m.SistemasModule) },
  { path: 'newsistema', loadChildren: () => import('./pages/new-sistema/new-sistema.module').then(m => m.NewSistemaModule) },
  { path: 'mysistem', loadChildren: () => import('./pages/mysistem/mysistem.module').then(m => m.MysistemModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
