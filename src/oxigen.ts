/*

option = {
  series: [
    {
      type: 'gauge',
      center: ['50%', '60%'],
      startAngle: 190,
      endAngle: -10,

      min: 0,
      max: 100,
      //divisoes do total
      splitNumber: 5,
      itemStyle: {
        //cor do nivel
        color: '#59EB57'
      },
      progress: {
        show: true,
        width: 30
      },

      pointer: {
        show: false
      },
      axisLine: {
        lineStyle: {
          width: 30
        }
      },
      axisTick: {
        show: false,
        distance: -45,
        splitNumber: 1,
        lineStyle: {
          width: 2,
          color: '#000000'
        }
      },
      splitLine: {
        show: false,
        distance: -50,
        length: 0,
        lineStyle: {
          width: 3,
          color: '#999'
        }
      },
      axisLabel: {
        show: false,
        distance: -20,
        color: '#000000',
        fontSize: 20
      },
      anchor: {
        show: false
      },
      title: {
        show: false
      },
      detail: {
        valueAnimation: true,
        width: '60%',
        lineHeight: 40,
        borderRadius: 8,
        offsetCenter: [0, '-15%'],
        fontSize: 60,
        fontWeight: 'bolder',
        formatter: '{value} %',
        color: 'inherit'
      },
      data: [
        {
          value: 20
        }
      ]
    },
  ]
};

setInterval(function () {
  const random = +(Math.random() * 80).toFixed(2);
  myChart.setOption<echarts.EChartsOption>({
    series: [
      {
        data: [
          {
            value: random
          }
        ]
      },
    ]
  });
}, 5000);

*/
